# !/usr/bin/env/python
# -*- coding: utf-8 -*-

import random
import time

import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
from sklearn.preprocessing import StandardScaler
from sklearn.utils import shuffle


def MaxMinNormalization(num, Max, Min):
    num = (num - Min) / (Max - Min)
    return num


def Z_ScoreNormalization(num, mu, sigma):
    num = (num - mu) / sigma
    return num


seed = int(time.time_ns())
random.seed(seed)

np.set_printoptions(suppress=True, precision=20, threshold=10, linewidth=40)  # np禁止科学计数法显示
pd.set_option('display.float_format', lambda x: '%.2f' % x)  # pd禁止科学计数法显示

df_all = pd.read_csv(r'result_30000.csv')

"""
remain_list = ['FlowIATMax',
                'FlowIATMin',
                'BwdPacketLengthStd',
                'FINFlagCount',
                'FwdIATTotal',
                'BwdIATTotal',
                'IdleMax',
                'Init_Win_bytes_forward',
                'min_seg_size_forward',
                'Label']

for i in df_all:
    if i not in remain_list:
        df_all = df_all.drop(i, axis=1)

print(df_all)


df_all_labels = df_all["Label"].copy()
df_all.drop(["Label"], axis=1, inplace=True)
y = df_all['Label']
X = df_all.drop(columns=['Label'], axis=1)
"""
X = df_all.iloc[:, :-1].values
y = df_all.iloc[:, -1].values
y = y.astype('int')
X, y = shuffle(X, y)
scaler = StandardScaler()
X = scaler.fit_transform(X)
"""from sklearn.feature_selection import VarianceThreshold

vt = VarianceThreshold(threshold=0)
X = vt.fit_transform(X)"""
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=27)

# 创建KNeighborsClassifier对象，并设置k值
knn = KNeighborsClassifier(n_neighbors=4, weights='distance', algorithm='auto', leaf_size=30,
                           p=2, metric='minkowski', metric_params=None, n_jobs=None)  # 设置k=4

# 使用训练集对模型进行训练
knn.fit(X_train, y_train)

# 使用测试集进行预测
y_pred = knn.predict(X_test)

# 计算预测准确率
accuracy = accuracy_score(y_test, y_pred)
precision = precision_score(y_test, y_pred)
recall = recall_score(y_test, y_pred)
f1 = f1_score(y_test, y_pred)

print("Accuracy:", accuracy)
print("Precision:", precision)
print("Recall:", recall)
print("F1 score:", f1)

from sklearn.metrics import classification_report, confusion_matrix

print(confusion_matrix(y_test, y_pred))
print('\n')
print(classification_report(y_test, y_pred))

import scikitplot as skplt
import matplotlib.pyplot as plt
skplt.metrics.plot_confusion_matrix(y_test, y_pred, normalize=True)
plt.show()
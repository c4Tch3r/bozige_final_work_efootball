# 用于绘制混淆矩阵，将此段代码复制到程序末尾即可

import scikitplot as skplt
import matplotlib.pyplot as plt

skplt.metrics.plot_confusion_matrix(y_test, y_pred, normalize=True)
plt.show()
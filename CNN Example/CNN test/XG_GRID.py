# !/usr/bin/env/python
# -*- coding: utf-8 -*-

import random
import time
from tqdm import tqdm

import numpy as np
import pandas as pd
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.utils import shuffle
from xgboost import XGBClassifier

seed = int(time.time_ns())
random.seed(seed)

np.set_printoptions(suppress=True, precision=20, threshold=10, linewidth=40)  # np禁止科学计数法显示
pd.set_option('display.float_format', lambda x: '%.2f' % x)  # pd禁止科学计数法显示

df_all = pd.read_csv(r'result_real.csv')

param_grid = {
    'booster': ['gbtree'],
    'objective': ['reg:linear'],
    'eval_metric': ['error'],
    'gamma': [1],
    'min_child_weight': [1.1],
    'max_depth': [20],
    'subsample': [0.8],
    'colsample_bytree': [0.8],
    'tree_method': ['exact'],
    'learning_rate': [0.1],
    'n_estimators': [200],
    'nthread': [4],
    'scale_pos_weight': [1],
    'seed': [27]
}

X = df_all.iloc[:, :-1].values
y = df_all.iloc[:, -1].values
y = y.astype('int')
X, y = shuffle(X, y)
scaler = StandardScaler()
X = scaler.fit_transform(X)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3, random_state=0)

xgb = XGBClassifier()
grid_search = GridSearchCV(estimator=xgb, param_grid=param_grid, cv=5)
grid_search.fit(X_train, y_train)
print("最优参数组合：", grid_search.best_params_)


y_pred = grid_search.predict(X_test)

accuracy = accuracy_score(y_test, y_pred)
precision = precision_score(y_test, y_pred)
recall = recall_score(y_test, y_pred)
f1 = f1_score(y_test, y_pred)

print("Accuracy:", accuracy)
print("Precision:", precision)
print("Recall:", recall)
print("F1 score:", f1)


print(confusion_matrix(y_test, y_pred))
print('\n')
print(classification_report(y_test, y_pred))

# Step1：特征采集

## cicflowmeter特征采集

https://www.freebuf.com/articles/network/279190.html

https://www.anquanke.com/post/id/207835

## Zeek
zeek-flow-master/：Datacon2020 TLS加密流量检测特征提取脚本


# Step2：数据清洗

## 基于WOE的二分类数据清洗

第一篇：https://zhuanlan.zhihu.com/p/518536607

第二篇：https://zhuanlan.zhihu.com/p/577485601

第三篇：https://zhuanlan.zhihu.com/p/594942152


# Step3：CNN搭建

CNN Example/main.py：一个乱搭的测试网络（基本没用），检查库配置和版本运行情况，1:300+特征二分类，选取src ip其他one-hot处理。

但是one-hot我不大喜欢，太臃肿了，尤其是那个鬼flowID。

之前看过一篇文章讲了怎么pandas怎么处理str类型，结果忘存了，你们再搜下。

```bash
pip install -r requirements.txt -i https://pypi.tuna.tsinghua.edu.cn/simple
```

如果要在这个基础上改，考虑是否加监督学习。

需要炼丹手。

# 已有项目

USTC：https://github.com/echowei/DeepTraffic

BUPT：https://github.com/RaidriarB/PythonSparkMachineLearningTest-backend

BUAA：https://github.com/talshapira/FlowPic

Datacon2020 pcap+Zeek+XGBoost：https://zhuanlan.zhihu.com/p/262533938

HawkEye Datacon：https://geekdaxue.co/read/jianouzuihuai@cyber-security/zxb3um